# OpenXball

A few people in the paintball industry lost their collective minds and seriously think that a glorified kitchen timer and a couple of buttons (that have all sorts of bugs btw) is seriously worth at least $1k.

I know that we spend thousands on guns and gear, but this is a complete racket. Along with the APPA ID's.

So let's fix one problem and make Speedball software that can run BETTER than anything PBResults can build AND can run on a Raspberry Pi.


## Getting Started

Devs can download the source...
Regular peeps can download the JAR when I'm ready...

### Prerequisites

You need a Raspberry Pi, Speaker Wire (which can be purchased at your local hardware store), and two buttons. Doorbells work just as well.

### Installing

We're not there yet...

## Deployment

We're not there yet.

## Built With

* Java

## License

This project is licensed under the Mozilla Public License

## Acknowledgments

* APPA for gauging players for ID's
* PBResults for gauging fields & organizers for their software/hardware

I wouldn't have been inspired to do this without you guys. <3
